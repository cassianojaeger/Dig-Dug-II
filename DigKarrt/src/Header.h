/***
///INTS
/**
Screen dimensions
*/
int windowWidth = 600;
int windowHeight = 480;

/**
Screen position
*/
int windowXPos = 100;
int windowYPos = 150;
int mainWindowId = 0;
int matrixRGBA[20][20][4];//matrix com os valores do bitmap
int mouseLastX = 0;
int mouseLastY = 0;
int zAux = -19;
int xAux = -19;
int numTextures;
int visao = 0;
int auxA = 0;
int auxCap = 0;
int qntRotate = 0;
int pontuacao = 0;
bool empurrarMonstro = false;
int empurrarDenovo = 2;
bool empurrarMonstroTimer = false;
///BOOLS
bool upPressed = false;
bool downPressed = false;
bool randerModels = true;
bool spacePressed = false;
bool twoD = false; /// variavel da visão 2D
bool running = false;
bool jumping = false;
bool rotateA = false;
bool rotateD = false;
bool thirdPerson = false;
bool menuRender = true;
///FLOATS
float roty = 0.0f;
float rotx = 90.0f;
float speedX = 0.0f;
float speedY = 0.0f;
float speedZ = 0.0f;
float posX = 0.20f;
float posY = 0.5f;
float posZ = 2.0f;
float posXBoneco;
float posZBoneco;
float posYBoneco;
float speedRotation = 1;
float altura = 0.0f;
float aresta = 2.0f;
float jumpSpeed = 0.06;
float gravity = 0.004;
float heightLimit = 0.7;
float posYOffset = 0.2;
float rotCap = 180;
float rotCapAntigo = 180;
double xOffset = -1.9;
double yOffset = -1.3;
float XBoneco;
float ZBoneco;
int gambiMorte = 0;
bool inimigosAndando = false;
int auxGambi = 0;
bool personagemMorto = false;
bool personagemMortoInimigo = false;
int verificaBuraco = 0;
int andarZ = 0;
int andarX = 0;
bool naoAcabou = true;
float posXAnterior = 0.01f;
float posZAnterior = 2.0f;
float rotyAnterior = 0;
float rotxAnterior = 0;
float animacaoMorte = 0;
int seedX = 0;
int seedZ = 0;
int qntBlocosColoridos = 0;
bool testaFloodFill = false;
int qntDeUns = 0;
bool ciclo = 0;


typedef struct pontos{
	int x;
	int y;
	int direcaoX; ///DIREÇÕES PARA FAZER A MOVIMENTAÇÃO SUAVE DO INIMIGO NO MAPA
	int direcaoZ; /// [-2,2] => gambi para ele andar e também servir como parametro de decisão de direção
	float offsetX = 0;
	float offsetZ = 0;
	bool vivo = true;
}posXY;

typedef struct personagem {
    int x;
    int z;
    int direcaoX = 0;
    int direcaoZ = -2;
}personagem;

posXY posInimigos[4];
personagem mainChar;
bool terminouVerificacao = true;
int quantidadeBuraco = 2;
int tempoDePartida = 60;
int contador = tempoDePartida;
int restart = 5;

char mapName;
int lowerMap[20][20][4];
int upperMap[20][20][4];
/*variavel auxiliar pra dar variação na altura do ponto de vista ao andar.
*/
float headPosAux = 0.0f;

float maxSpeed = 0.25f;

float planeSize = 8.0f;

// more sound stuff (position, speed and orientation of the listener)
ALfloat listenerPos[]={0.0,0.0,4.0};
ALfloat listenerVel[]={0.0,0.0,0.0};
ALfloat listenerOri[]={0.0,0.0,1.0,
						0.0,1.0,0.0};

// now the position and speed of the sound source
ALfloat source0Pos[]={ -2.0, 0.0, 0.0};
ALfloat source0Vel[]={ 0.0, 0.0, 0.0};

// buffers for openal stuff
ALuint  buffer[NUM_BUFFERS];
ALuint  source[NUM_SOURCES];
ALuint  environment[NUM_ENVIRONMENTS];
ALsizei size,freq;
ALenum  format;
ALvoid  *data;



// parte de código extraído de "texture.c" por Michael Sweet (OpenGL SuperBible)
// texture buffers and stuff
int i;                       /* Looping var */
BITMAPINFO	*info;           /* Bitmap information */
GLubyte	    *bits;           /* Bitmap RGB pixels */
GLubyte     *ptr;            /* Pointer into bit buffer */
GLubyte	    *rgba;           /* RGBA pixel buffer */
GLubyte	    *rgbaptr;        /* Pointer into RGBA buffer */
GLubyte     temp;            /* Swapping variable */
GLuint *textures = new GLuint[3];/*Texture object */


float backgrundColor[4] = {0.0f,0.0f,0.0f,1.0f};
GLMmodel *modelOBJText;
GLMmodel *modelOBJGrass;
GLMmodel *modelOBJInimigos;

///HEADERS DO TRABALHO///
void mainInit();
void initSound();
void initTexture();
void initModel();
void onMouseButton(int button, int state, int x, int y);
void onKeyDown(unsigned char key, int x, int y);
void onKeyUp(unsigned char key, int x, int y);
void mainCreateMenu();
void onGLUIEvent(int id);
void onWindowReshape(int x, int y);
void mainIdle();
void setViewport(GLint left, GLint right, GLint bottom, GLint top);
void createGLUI();
void mainRender();
int main(int argc, char **argv);
void setWindow();
void updateState();
void renderFloor();
void updateCam(int camMode);
void renderMaps(char mapName[], int matrixRGBA[20][20][4]);
void randerBMPModels();
void randerGrass(int xAux,int zAux);
void loadTexture(char bmpName[], int nTexture);
void renderCubes(int text1, int text2);
void verificaColisao(float posx, float posz, float speedXis, float speedZe);
void stroke_output(GLfloat x, GLfloat y, char *format,...);
bool posValida(int loop);
bool pathFinding();
void animacaoDeMorte();
void criaRachadura();
void defineDirecaoPersonagem();
void cavarBuraco();
void destruirBuraco();
void stroke_output(GLfloat x, GLfloat y,GLfloat z, char *format,...);
void placar();
void Timer(int value);
void determinaSeed();
bool floodFill(int seedz, int seedx);
bool determinaCiclo();
void derrubaChao();
void limpaLixo();
void verificaPontos();
void escritaMorte();
void mensagem( char *p);
void vitoria();
void verificaEstadoJogo();
void verificaEstadoJogador();
void empurrar();
void sunLight();

///LOAD NOS OBJETOS USADOS
bool C3DObject_Load_New(const char *pszFilename, GLMmodel **model)
{
    char aszFilename[256];
    strcpy(aszFilename, pszFilename);

    if (*model) {

    free(*model);
    *model = NULL;
    }

    *model = glmReadOBJ(aszFilename);
    if (!(*model))
    return false;

    glmUnitize(*model);
    //glmScale(model,sFactor); // USED TO SCALE THE OBJECT
    glmFacetNormals(*model);
    glmVertexNormals(*model, 90.0);
    return true;
}
///DETERMINA A INICIALIZAÇÃO DA CAMERA
void setWindow() {

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
    gluPerspective(45.0f,(GLfloat)windowWidth/(GLfloat)windowHeight,0.1f, 100.0f);
    glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(posX,posY + posYOffset + 0.025 * std::abs(sin(headPosAux*PI/180)),posZ,
		posX + sin(roty*PI/180),posY + posYOffset + 0.025 * std::abs(sin(headPosAux*PI/180)) + cos(rotx*PI/180),posZ -cos(roty*PI/180),
		0.0,1.0,0.0);
}
///NOSSA ORTHO
void newOrtho( GLdouble left,GLdouble right,GLdouble bottom,GLdouble top,GLdouble nearVal,GLdouble farVal)
{ /// Nova matrix na função ortho
    GLdouble orthoMatrix[16] = {0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,1.0f};
    orthoMatrix [0] = 2.0f/(right-left);
    orthoMatrix [12] = -((right+left)/(right-left));
    orthoMatrix [5] = 2.0f/(top-bottom);
    orthoMatrix [13] = -((top+bottom)/(top-bottom));
    orthoMatrix [10] = -2.0f/(farVal-nearVal);
    orthoMatrix [14] = -((farVal+nearVal)/(farVal-nearVal));
    glMultMatrixd(orthoMatrix);
}

///FUNÇÃO QUE CARREGA AS TEXTURAS PARA O OPENGL
void loadTexture(char bmpName[],int nTexture)
{
    printf ("\nLoading texture..\n");
    // Load a texture object (256x256 true color)
    bits = LoadDIBitmap(bmpName, &info);
    if (bits == (GLubyte *)0) {
		printf ("Error loading texture!\n\n");
		return;
	}
    // Create an RGBA image
    rgba = (GLubyte *)malloc(info->bmiHeader.biWidth * info->bmiHeader.biHeight * 4);

    i = info->bmiHeader.biWidth * info->bmiHeader.biHeight;
    for( rgbaptr = rgba, ptr = bits;  i > 0; i--, rgbaptr += 4, ptr += 3)
    {
            rgbaptr[0] = ptr[2];     // windows BMP = BGR
            rgbaptr[1] = ptr[1];
            rgbaptr[2] = ptr[0];
            rgbaptr[3] = (ptr[0] + ptr[1] + ptr[2]) / 3;
    }
    glBindTexture(GL_TEXTURE_2D, textures[nTexture]);

	// Set texture parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    glTexImage2D(GL_TEXTURE_2D, 0, 4, info->bmiHeader.biWidth, info->bmiHeader.biHeight,
                  0, GL_RGBA, GL_UNSIGNED_BYTE, rgba );
}
///FUNÇÃO QUE CRIA UM CUBO TEXTURIZADO
void renderCubes(int text1, int text2)
{
   ///CRIAR UM CUBO E TEXTURIZAR COM AS TEXTURES[0] E TEXTURES[1]
    glShadeModel(GL_SMOOTH);
	glEnable(GL_TEXTURE_2D);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    glBindTexture(GL_TEXTURE_2D, textures[text1]);
    glBegin(GL_QUADS);
        glTexCoord2f(1.0f, 0.0f);   // coords for the texture
        glNormal3f(0.0f,1.0f,0.0f);
        glVertex3f(0.0f, aresta, aresta);

        glTexCoord2f(0.0f, 0.0f);  // coords for the texture
        glNormal3f(0.0f,1.0f,0.0f);
        glVertex3f(aresta, aresta, aresta);

        glTexCoord2f(0.0f, 1.0f);  // coords for the texture
        glNormal3f(0.0f,1.0f,0.0f);
        glVertex3f(aresta, aresta, 0.0f);

        glTexCoord2f(1.0f, 1.0f);  // coords for the texture
        glNormal3f(0.0f,1.0f,0.0f);
        glVertex3f(0.0f, aresta, 0.0f);
    glEnd();
    //RIGHT SIDE
    glBindTexture(GL_TEXTURE_2D, textures[text2]);
    glBegin(GL_QUADS);
        glTexCoord2f(1.0f, 0.0f);   // coords for the texture
        glNormal3f(1.0f,0.0f,0.0f);
        glVertex3f(aresta,0.0f,0.0f);

        glTexCoord2f(0.0f, 0.0f);  // coords for the texture
        glNormal3f(1.0f,0.0f,0.0f);
        glVertex3f(aresta,0.0f,aresta);

        glTexCoord2f(0.0f, 1.0f);  // coords for the texture
        glNormal3f(1.0f,0.0f,0.0f);
        glVertex3f(aresta,aresta,aresta);

        glTexCoord2f(1.0f, 1.0f);  // coords for the texture
        glNormal3f(1.0f,0.0f,0.0f);
        glVertex3f(aresta,aresta,0.0f);
    glEnd();
    //DOWN SIDE
    glBegin(GL_QUADS);
        glTexCoord2f(1.0f, 0.0f);   // coords for the texture
        glNormal3f(0.0f,-1.0f,0.0f);
        glVertex3f(0.0f, 0.0f, 0.0f);

        glTexCoord2f(0.0f, 0.0f);  // coords for the texture
        glNormal3f(0.0f,-1.0f,0.0f);
        glVertex3f(aresta,0.0f,0.0f);

        glTexCoord2f(0.0f, 1.0f);  // coords for the texture
        glNormal3f(0.0f,-1.0f,0.0f);
        glVertex3f(aresta,0.0f,aresta);

        glTexCoord2f(1.0f, 1.0f);  // coords for the texture
        glNormal3f(0.0f,-1.0f,0.0f);
        glVertex3f(0.0f,0.0f,aresta);
    glEnd();
    //BACK SIDE
    glBegin(GL_QUADS);
        glTexCoord2f(1.0f, 0.0f);   // coords for the texture
        glNormal3f(0.0f,0.0f,-1.0f);
        glVertex3f(0.0f, 0.0f, 0.0f);

        glTexCoord2f(0.0f, 0.0f);  // coords for the texture
        glNormal3f(0.0f,0.0f,-1.0f);
        glVertex3f(aresta,0.0f,0.0f);

        glTexCoord2f(0.0f, 1.0f);  // coords for the texture
        glNormal3f(0.0f,0.0f,-1.0f);
        glVertex3f(aresta,aresta,0.0f);

        glTexCoord2f(1.0f, 1.0f);  // coords for the texture
        glNormal3f(0.0f,0.0f,-1.0f);
        glVertex3f(0.0f,aresta,0.0f);
    glEnd();
     //LEFT SIDE
    glBegin(GL_QUADS);
        glTexCoord2f(1.0f, 0.0f);   // coords for the texture
        glNormal3f(-1.0f,0.0f,0.0f);
        glVertex3f(0.0f, 0.0f, 0.0f);

        glTexCoord2f(0.0f, 0.0f);  // coords for the texture
        glNormal3f(-1.0f,0.0f,0.0f);
        glVertex3f(0.0f,0.0f,aresta);

        glTexCoord2f(0.0f, 1.0f);  // coords for the texture
        glNormal3f(-1.0f,0.0f,0.0f);
        glVertex3f(0.0f,aresta,aresta);

        glTexCoord2f(1.0f, 1.0f);  // coords for the texture
        glNormal3f(-1.0f,0.0f,0.0f);
        glVertex3f(0.0f,aresta,0.0f);
    glEnd();
     //FRONT SIDE
    glBegin(GL_QUADS);
        glTexCoord2f(1.0f, 0.0f);   // coords for the texture
        glNormal3f(0.0f,0.0f,1.0f);
        glVertex3f(0.0f,0.0f,aresta);

        glTexCoord2f(0.0f, 0.0f);  // coords for the texture
        glNormal3f(0.0f,0.0f,1.0f);
        glVertex3f(aresta,0.0f,aresta);

        glTexCoord2f(0.0f, 1.0f);  // coords for the texture
        glNormal3f(0.0f,0.0f,1.0f);
        glVertex3f(aresta,aresta,aresta);

        glTexCoord2f(1.0f, 1.0f);  // coords for the texture
        glNormal3f(0.0f,0.0f,1.0f);
        glVertex3f(0.0f,aresta,aresta);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}
///DEIXEI PQ N SEI SE É IMPORTANTE PRO JOGO HAHHAHA
void mainHandleMouseRightButtonMenuEvent(int option) {
	switch (option) {
		case 1 :
			exit(0);
			break;
		default:
			break;
	}
}
///MAIN CREATE MENU
void mainCreateMenu() {
	glutCreateMenu(mainHandleMouseRightButtonMenuEvent);
	glutAddMenuEntry("Quit", 1);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}
///MOUSE HANDLER
void onMouseButton(int button, int state, int x, int y)
{
    if(button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
    {

        PlaySound("res/tirarBuraco.wav", NULL, SND_ASYNC|SND_FILENAME);
        destruirBuraco();
    }

    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
    {
        PlaySound("res/criarBuraco.wav", NULL, SND_ASYNC|SND_FILENAME);
        cavarBuraco();
        if(testaFloodFill)
        {
            determinaSeed();
            //printf("x:%d z:%d", seedX, seedZ);
            floodFill(seedZ, seedX);
            ///SE HOUVER CICLO, DERRUBAMOS O CHAO
            if(determinaCiclo())
            {
                derrubaChao();
                limpaLixo();
            }
            ///REINICIALIZAMOS AS VARIAVEIS
            qntBlocosColoridos = 0;
            qntDeUns = 0;
            ciclo = false;
            testaFloodFill = false;
        }
    }

}
///WINDOWS RESHAPE
void onWindowReshape(int x, int y)
{
	windowWidth = x;
	windowHeight = y;
	setWindow();
	setViewport(0, windowWidth, 0, windowHeight);
}
///GLUT IDLE FUNCTION
void mainIdle()
{
	/**
	Set the active window before send an glutPostRedisplay call
	so it wont be accidently sent to the glui window
	*/

	glutSetWindow(mainWindowId);
	glutPostRedisplay();
}
///FUNÇÃO QUE ROA O MODELO DO PERSONAGEM PRINCIPAL
void defineDirecaoPersonagem()
{
    if(rotCap == 450)
    {
        rotCap = 90;
    }
    if(rotCap == -90)
    {
        rotCap = 270;
    }
    if(rotCap == 180)
    {
        mainChar.direcaoX = 0;
        mainChar.direcaoZ = -2;
    }
    if(rotCap == 90)
    {
        mainChar.direcaoX = 2;
        mainChar.direcaoZ = 0;
    }
    if(rotCap == 0 || rotCap == 360)
    {
        mainChar.direcaoX = 0;
        mainChar.direcaoZ = 2;
    }
    if(rotCap == 270)
    {
        mainChar.direcaoX = -2;
        mainChar.direcaoZ = 0;
    }
}
///FUNÇÃO QUE FAZ A ANIMAÇÃO DE ROTAÇÃO DOS INIMIGOS
void rotaInimigos(int inimigoAux)
{
    if(posInimigos[inimigoAux].direcaoZ == -2 && posInimigos[inimigoAux].direcaoX == 0)
    {
        glRotatef(180, 0.0f, 1.0f, 0.0f);
    }
    if(posInimigos[inimigoAux].direcaoZ == 0 && posInimigos[inimigoAux].direcaoX == 2)
    {
        glRotatef(90, 0.0f, 1.0f, 0.0f);
    }
    if(posInimigos[inimigoAux].direcaoZ == 0 && posInimigos[inimigoAux].direcaoX == -2)
    {
        glRotatef(-90, 0.0f, 1.0f, 0.0f);
    }
}
///FAZ A VERIFICAÇÃO SE EXISTE PATHFINDING NA DECISÃO DOS INIMIGOS
bool pathFinding()
{
    for(int i = 0; i<=3; i++)
    {
        for(int j=i+1; j<=3; j++)
        {
            if(i == 3)
            {
                i = 4;
                j = 4;
            }
            if(((posInimigos[i].x+(posInimigos[i].direcaoX/2) == posInimigos[j].x+(posInimigos[j].direcaoX/2))
                &&(posInimigos[i].y+(posInimigos[i].direcaoZ/2) == posInimigos[j].y+(posInimigos[j].direcaoZ/2)))
               || ((posInimigos[i].x+(posInimigos[i].direcaoX/2) == posInimigos[j].x)
                &&(posInimigos[i].y+(posInimigos[i].direcaoZ/2) == posInimigos[j].y)))
            {
                return true;
            }
        }
    }
    return false;
}
///FUNÇÃO QUE DETERMINAR SE O INIMIGO DECIDIU IR PARA UM BLOCO VÁLIDO
bool posValida(int loop)
{///VERIFICA DIREÇÃO SUL
    if(posInimigos[loop].direcaoZ == 2 && posInimigos[loop].direcaoX == 0)
    {
        if(upperMap[posInimigos[loop].y+1][posInimigos[loop].x][0] == 127
           || upperMap[posInimigos[loop].y+1][posInimigos[loop].x][0] == 90
           || upperMap[posInimigos[loop].y+1][posInimigos[loop].x][0] == 150
           || lowerMap[posInimigos[loop].y+1][posInimigos[loop].x][1] == 0
           || posInimigos[loop].y+1 == 20)
        {
            return false;
        }
    }///VERIFICA DIREÇÃO DIREITA
    if(posInimigos[loop].direcaoZ == 0 && posInimigos[loop].direcaoX == 2)
    {
        if(upperMap[posInimigos[loop].y][posInimigos[loop].x+1][0] == 127
           || upperMap[posInimigos[loop].y][posInimigos[loop].x+1][0] == 90
           || upperMap[posInimigos[loop].y][posInimigos[loop].x+1][0] == 150
           || lowerMap[posInimigos[loop].y][posInimigos[loop].x+1][1] == 0
           || posInimigos[loop].x+1 == 20)
        {
            return false;
        }
    }///VERIFICA DIREÇÃO NORTE
    if(posInimigos[loop].direcaoZ == -2 && posInimigos[loop].direcaoX == 0)
    {
        if(upperMap[posInimigos[loop].y-1][posInimigos[loop].x][0] == 127
           || upperMap[posInimigos[loop].y-1][posInimigos[loop].x][0] == 90
           || upperMap[posInimigos[loop].y-1][posInimigos[loop].x][0] == 150
           || lowerMap[posInimigos[loop].y-1][posInimigos[loop].x][1] == 0
           || posInimigos[loop].y-1 == -1)
        {
            return false;
        }
    }///VERIFICA DIREÇÃO ESQUERDA
    if(posInimigos[loop].direcaoZ == 0 && posInimigos[loop].direcaoX == -2)
    {
        if(upperMap[posInimigos[loop].y][posInimigos[loop].x-1][0] == 127
           || upperMap[posInimigos[loop].y][posInimigos[loop].x-1][0] == 90
           || upperMap[posInimigos[loop].y][posInimigos[loop].x-1][0] == 150
           || lowerMap[posInimigos[loop].y][posInimigos[loop].x-1][1] == 0
           || posInimigos[loop].x-1 == -1)
        {
            return false;
        }
    }
    return true;
}
///FUNÇÃO QUE FAZ A MOVIMENTAÇÃO DOS INIMIGOS
void movimentaInimigos()
{///FOR QUE VARRE O ARRAY DE INIMIGOS
    for(int loop = 0; loop <= 3; loop++)
    {///SE O MONSTRO ESTIVER VIVO E A POSIÇÃO ESCOLHIDA FOR VÁLIDA, ELE IRÁ VERIFICAR SE JÁ TERMINOU DE SE MOVIMENTAR
        if(posInimigos[loop].vivo && posValida(loop))
        {///VERIFICA SE O MONSTRO AINDA PRECISA SE MOVIMENTAR
            if((posInimigos[loop].offsetZ < 2 && posInimigos[loop].offsetX == 0 && posInimigos[loop].offsetZ >= 0)
               || (posInimigos[loop].offsetZ > -2 && posInimigos[loop].offsetX == 0 && posInimigos[loop].offsetZ <= 0)
               || (posInimigos[loop].offsetZ == 0 && posInimigos[loop].offsetX < 2 && posInimigos[loop].offsetX >= 0)
               || (posInimigos[loop].offsetZ == 0 && posInimigos[loop].offsetX > -2 && posInimigos[loop].offsetX <=0))
            {
                ///SUL
                if(posInimigos[loop].direcaoZ == 2 && posInimigos[loop].direcaoX == 0)
                {
                    posInimigos[loop].offsetZ += 0.05;
                }///DIREITA
                if(posInimigos[loop].direcaoZ == 0 && posInimigos[loop].direcaoX == 2)
                {
                    posInimigos[loop].offsetX += 0.05;
                }///NORTE
                if(posInimigos[loop].direcaoZ == -2 && posInimigos[loop].direcaoX == 0)
                {
                    posInimigos[loop].offsetZ -= 0.05;
                }///ESQUERDA
                if(posInimigos[loop].direcaoZ == 0 && posInimigos[loop].direcaoX == -2)
                {
                    posInimigos[loop].offsetX -= 0.05;
                }
            }
            else
            {
                printf("\n");
                ///INIMIGO ATUAL TERMINOU DE SE MOVIMENTAR
                for(int i = 0; i<=3; i++)
                {///ZERA OS OFFSETS DE MOVIMENTAÇÃO AUXILIAR
                    ///E ATUALIZA O GRID PARA MOVER OS INIMIGOS NA DIREÇÃO
                    ///SUL
                    if((posInimigos[i].direcaoZ == 2) && (posInimigos[i].direcaoX == 0)
                       && (posInimigos[i].offsetZ > 2))
                    {
                        if(lowerMap[posInimigos[i].y+1][posInimigos[i].x][0] == 0
                           && lowerMap[posInimigos[i].y+1][posInimigos[i].x][1] != 255)
                        {
                        }
                        else
                        {
                            upperMap[posInimigos[i].y+1][posInimigos[i].x][0] = 255;
                            upperMap[posInimigos[i].y+1][posInimigos[i].x][3] = 2;
                        }
                        if(upperMap[posInimigos[i].y][posInimigos[i].x][3] == 0)
                            upperMap[posInimigos[i].y][posInimigos[i].x][0] = 0;
                    }///DIREITA
                    if((posInimigos[i].direcaoZ == 0) && (posInimigos[i].direcaoX == 2)
                       && (posInimigos[i].offsetX > 2))
                    {
                        if(lowerMap[posInimigos[i].y][posInimigos[i].x+1][0] == 0
                           && lowerMap[posInimigos[i].y][posInimigos[i].x+1][1] != 255)
                        {
                        }
                        else
                        {
                            upperMap[posInimigos[i].y][posInimigos[i].x+1][0] = 255;
                            upperMap[posInimigos[i].y+1][posInimigos[i].x][3] = 2;
                        }
                        if(upperMap[posInimigos[i].y][posInimigos[i].x][3] == 0)
                            upperMap[posInimigos[i].y][posInimigos[i].x][0] = 0;
                    }///NORTE
                    if((posInimigos[i].direcaoZ == -2) && (posInimigos[i].direcaoX == 0)
                       && (posInimigos[i].offsetZ < -2))
                    {
                        if(lowerMap[posInimigos[i].y-1][posInimigos[i].x][0] == 0
                           && lowerMap[posInimigos[i].y-1][posInimigos[i].x][1] != 255)
                        {
                        }
                        else
                        {
                            upperMap[posInimigos[i].y-1][posInimigos[i].x][0] = 255;
                            upperMap[posInimigos[i].y+1][posInimigos[i].x][3] = 2;
                        }
                        if(upperMap[posInimigos[i].y][posInimigos[i].x][3] == 0)
                            upperMap[posInimigos[i].y][posInimigos[i].x][0] = 0;
                    }///ESQUERDA
                    if((posInimigos[i].direcaoZ == 0) && (posInimigos[i].direcaoX == -2)
                       && (posInimigos[i].offsetX < -2))
                    {
                        if(lowerMap[posInimigos[i].y][posInimigos[i].x-1][0] == 0
                           && lowerMap[posInimigos[i].y][posInimigos[i].x-1][1] != 255)
                        {
                        }
                        else
                        {
                            upperMap[posInimigos[i].y][posInimigos[i].x-1][0] = 255;
                            upperMap[posInimigos[i].y+1][posInimigos[i].x][3] = 2;
                        }
                        if(upperMap[posInimigos[i].y][posInimigos[i].x][3] == 0)
                            upperMap[posInimigos[i].y][posInimigos[i].x][0] = 0;
                    }
                    ///ZERA AS VARIAVEIS DE CAMINHAMENTO REAL DOS INIMIGOS
                    posInimigos[i].offsetX = 0;
                    posInimigos[i].offsetZ = 0;
                }
                ///VARIAVEL PARA DETERMINAR QUE OS MONSTROS DEVEM ESCOLHER NOVAS DIREÇÕES
                inimigosAndando = false;
                for (int i = 0; i<20;i++)
                {
                    for(int j = 0;j<20;j++)
                    {
                        upperMap[i][j][3] = 0;
                    }
                }
            }
        }
    }
}
///FUNÇÃO QUE FAZ AS ANIMAÇÕES DE MORTE
void animacaoDeMorte()
{
    if(personagemMorto)
    {
        glPushMatrix();
            glScalef(0.2f,0.2f,0.2f);
            posXBoneco = posX*5;
            posZBoneco = posZ*5;
            posYBoneco = posY+animacaoMorte+2.48;
            glTranslatef(posXBoneco,posYBoneco,posZBoneco);
            glRotatef(rotCap,0,1,0);
            glmDraw(modelOBJText, GLM_SMOOTH | GLM_MATERIAL | GLM_TEXTURE);
        glPopMatrix();
        animacaoMorte -= 0.025;
            if(posYBoneco < 1)
            {///AREA ONDE IREMOS FAZER O RENASCIMENTO DO PERSONAGEM DEPOIS DE MORRER
                if(gambiMorte == 0)
                {

                    PlaySound("res/morte.wav", NULL, SND_SYNC|SND_FILENAME);
                    mainInit();
                }
            }
    }
    else
    {///CASO BATA NO INIMIGO, O CARRO VIRA
       glPushMatrix();
            glScalef(0.2f,0.2f,0.2f);
            glTranslatef(posXAnterior*5,posYBoneco-0.5,posZAnterior*5);
            glRotatef(rotCapAntigo,0,1,0);
            glRotatef(180,0,0,1);
            glmDraw(modelOBJText, GLM_SMOOTH | GLM_MATERIAL | GLM_TEXTURE);
        glPopMatrix();
        if(gambiMorte == 0)
        {
            PlaySound("res/morte.wav", NULL, SND_SYNC|SND_FILENAME);
            mainInit();
        }
    }
}
///FUNÇÃO QUE CRIA A LUZ DO JOGO
void sunLight(){
    glEnable(GL_LIGHTING );
    glEnable(GL_LIGHT1);

    GLfloat light_ambient2[] = { backgrundColor[0], backgrundColor[1], backgrundColor[2], backgrundColor[3] };
	GLfloat light_diffuse2[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat light_specular2[] = { 0.0, 0.0, 0.0, 1.0 };
	GLfloat light_position2[] = {0.0, 0.0, 0.0, 1.0 };

	glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient2);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse2);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular2);
	glLightfv(GL_LIGHT1, GL_POSITION, light_position2);
}

void setViewport(GLint left, GLint right, GLint bottom, GLint top) {
	glViewport(left, bottom, right - left, top - bottom);
}
///RENDERIZA O CHAO
void renderFloor() {
	// set things up to render the floor with the texture
	glShadeModel(GL_SMOOTH);
	glEnable(GL_TEXTURE_2D);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glPushMatrix();

    glTranslatef(-(float)planeSize/2.0f, 0.0f, -(float)planeSize/2.0f);

	float textureScaleX = 10.0;
	float textureScaleY = 10.0;
    glColor4f(1.0f,1.0f,1.0f,1.0f);
    int xQuads = 40;
    int zQuads = 40;
    for (int i = 0; i < xQuads; i++) {
        for (int j = 0; j < zQuads; j++) {
            glBegin(GL_QUADS);
                glTexCoord2f(1.0f, 0.0f);   // coords for the texture
                glNormal3f(0.0f,1.0f,0.0f);
                glVertex3f(i * (float)planeSize/xQuads, 0.0f, (j+1) * (float)planeSize/zQuads);

                glTexCoord2f(0.0f, 0.0f);  // coords for the texture
                glNormal3f(0.0f,1.0f,0.0f);
                glVertex3f((i+1) * (float)planeSize/xQuads, 0.0f, (j+1) * (float)planeSize/zQuads);

                glTexCoord2f(0.0f, 1.0f);  // coords for the texture
                glNormal3f(0.0f,1.0f,0.0f);
                glVertex3f((i+1) * (float)planeSize/xQuads, 0.0f, j * (float)planeSize/zQuads);

                glTexCoord2f(1.0f, 1.0f);  // coords for the texture
                glNormal3f(0.0f,1.0f,0.0f);
                glVertex3f(i * (float)planeSize/xQuads, 0.0f, j * (float)planeSize/zQuads);
            glEnd();
        }
    }

	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}
///FUNÇÃO QUE ATUALIZA AS CAMERAS DO JOGO
void updateCam(bool minimapa) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if(minimapa)
    {
        if(visao == 2){ /// 2DVision
            newOrtho(-2,2,-2,2,7.0,100.5);
            glMatrixMode(GL_MODELVIEW);
            gluLookAt(posXAnterior+0.5,9.5f,posZAnterior,
                      posXAnterior,posY,posZAnterior,
                      0.0,1.0,0.0);
        }
        else
        {
            gluPerspective(45.0f,(GLfloat)windowWidth/(GLfloat)windowHeight,0.1f, 100.0f);
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();
            if(visao == 1)
            {///ThirdPersonVision
                gluLookAt(posXAnterior-sin(rotyAnterior*PI/180),posY+0.5 + posYOffset + 0.025 * std::abs(sin(PI/180)),posZAnterior+cos(rotyAnterior*PI/180),
                posXAnterior + sin(rotyAnterior*PI/180),posY-0.2 + posYOffset + 0.025 * std::abs(sin(PI/180)) + cos(rotxAnterior*PI/180),posZAnterior -cos(rotyAnterior*PI/180),
                0.0,1.0,0.0);
            }
            else
            {///FPSVision
                gluLookAt(posXAnterior,posY + posYOffset + 0.025 * std::abs(sin(headPosAux*PI/180)),posZAnterior,
                posXAnterior + sin(rotyAnterior*PI/180),posY-0.2 + posYOffset + 0.025 * std::abs(sin(headPosAux*PI/180)) + cos(rotxAnterior*PI/180),posZAnterior -cos(rotyAnterior*PI/180),
                0.0,1.0,0.0);
            }

        }
    }
    else
    {
        newOrtho(-2,2,-2,2,7.0,100.5);
        glMatrixMode(GL_MODELVIEW);
        gluLookAt(posX+0.5,9.5f,posZ,
                  posX,posY,posZ,
                  0.0,1.0,0.0);
    }
	// atualiza a posição do listener e da origen do som, são as mesmas da camera, já que os passos vem de onde o personagem está
	listenerPos[0] = posX;
	listenerPos[1] = posY;
	listenerPos[2] = posZ;
	source0Pos[0] = posX;
	source0Pos[1] = posY;
	source0Pos[2] = posZ;
}
///FUNÇÃO QUE DESENHA NA TELA
void stroke_output(GLfloat x, GLfloat y,GLfloat z, char *format,...)
{
  va_list args;
  char buffer[200], *p;

  va_start(args, format);
  vsprintf(buffer, format, args);
  va_end(args);
  glPushMatrix();
  glTranslatef(x, y, z);
  for (p = buffer; *p; p++)
    glutStrokeCharacter(GLUT_STROKE_ROMAN, *p);
  glPopMatrix();
}
///TIMER PARA O CONTADOR DO PLACAR
void Timer(int value)
{
    // Move o quadrado
    contador -= 1;
    if(pontuacao == 600)
    {
        restart -=1;
    }
    if(empurrarMonstroTimer)
    {
        empurrarDenovo++;
    }

    glutPostRedisplay();
    glutTimerFunc(1000,Timer, 1);
}
///FUNÇÃO QUE DESENHA O PLACAR
void placar()
{
        glPushMatrix();
            glTranslatef(-7,0,-8);
            glScalef(0.06, 0.02, 0.02); // diminui o tamanho do fonte
            glLineWidth(2); // define a espessura da linha
            stroke_output(-10, 0,-10, "%i ",contador);
        glPopMatrix();

        glPushMatrix();

            glTranslatef(-5,2.35,-10);
            glScalef(0.004, 0.002, 0.002); // diminui o tamanho do fonte
            stroke_output(-4.8, 2,-10, "Segundos Restantes ");
        glPopMatrix();
}
///CRIAR UM BURACO NO CHÃO
void cavarBuraco()
{
    if(upperMap[mainChar.z][mainChar.x][0] == 0 && quantidadeBuraco != 0)
    {
        quantidadeBuraco += -1;
        upperMap[mainChar.z][mainChar.x][0] = 150;
        upperMap[mainChar.z][mainChar.x][1] = 120;
        upperMap[mainChar.z][mainChar.x][2] = 80;
        lowerMap[mainChar.z][mainChar.x][3] = 0;
    }
}
///FUNÇÃO QUE DESTRIO UM BURACO DO CHÃO
void destruirBuraco()
{
    if(upperMap[mainChar.z][mainChar.x][0] == 150 && upperMap[mainChar.z][mainChar.x][1] == 120
        && upperMap[mainChar.z][mainChar.x][2] == 80)
    {
        quantidadeBuraco++;
        upperMap[mainChar.z][mainChar.x][0] = 0;
        upperMap[mainChar.z][mainChar.x][1] = 0;
        upperMap[mainChar.z][mainChar.x][2] = 0;
    }
}
///FUNÇÃO QUE VERIFICA SE O JOGO JÁ TERMINOU
void verificaEstadoJogo()
{
    verificaPontos();
    if(pontuacao == 600)
    {
        mensagem("Victory");
        PlaySound("res/win.wav", NULL, SND_ASYNC|SND_FILENAME|SND_LOOP);
        glPushMatrix();
          glRotatef(rotCapAntigo,0,1,0);
            glTranslatef(+1.0,0.1,2);
            glScalef(0.002, 0.001, 0.001); // diminui o tamanho do fonte
            glLineWidth(5); // define a espessura da linha
            glRotatef(180,0,270,0);
            stroke_output(0,0,0,"Restart in: %i ", restart);
        glPopMatrix();

        vitoria();
    }
}
