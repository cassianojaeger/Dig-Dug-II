/**
//PRINCIPAL
6-FAZER OS INIMIGOS ESCOLHEREM IR ATRÁS DE MIM

///*----------ATÉ SABADO ESSA PORRA TEM QUE TA PRONTA----------*\\\

OBS EXTRAS:
1-QUANDO DESTRUIMOS O CHÃO, AS RACHADURAS E BURACOS SOMEM
2-ARMA QUE CRIA E DISTROI BURACOS
3-CRONOMETRO


*/

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cmath>
#include <iostream>
#include <gl/glut.h>
#include "glm.h"
//openal (sound lib)
#include <al/alut.h>
//bitmap class to load bitmaps for textures
#include "bitmap.h"
#pragma comment(lib, "OpenAL32.lib")
#pragma comment(lib, "alut.lib")
#define PI 3.14159265
// sound stuff
#define NUM_BUFFERS 1
#define NUM_SOURCES 1
#define NUM_ENVIRONMENTS 1
// TEXTURES
#define SMOOTH 0
#define SMOOTH_MATERIAL 1
#define SMOOTH_MATERIAL_TEXTURE 2
//constantes
#define FOG_DENSITY 0.5f
#define FOG_START 5.0f
#define FOG_END 100.0f
#define CUTOFFLANTERN 10.0f
#define CUTOFFDEFAULT 30.0f
//headers e arquivos auxiliares
#include "Header.h"

//FUNÇÕES DE INICIALIZAÇÃO------------------------------------------------------------
/**
Initialize
*/
void mainInit() {
	glClearColor(1.0,1.0,1.0,0.0);
	glColor3f(0.0f,0.0f,0.0f);
	setWindow();
	setViewport(0, windowWidth, 0, windowHeight);

	// habilita o z-buffer
	glEnable(GL_DEPTH_TEST);

    initSound();

    ///INICIALIZA AS TEXTURAS
    initTexture();

    ///INICIALIZA OS MODELOS OBJ
	initModel();

    ///INICIALIZAÇÃO DA LUZ GLOBAL
	sunLight();
    posX = 0.01;
    posZ = 2;
    posY = 0.5;
    empurrarMonstro = false;
    personagemMorto = false;
    personagemMortoInimigo = false;
    animacaoMorte = 0;
    contador = tempoDePartida;
    pontuacao = 0;
    restart = 5;
    quantidadeBuraco = 2;
    gambiMorte = 0;


	renderMaps("res/lower_map.bmp", lowerMap);
    renderMaps("res/upper_map.bmp", upperMap);

	printf("w - andar \n");
	printf("s - ir pra tras \n");
	printf("r - correr \n");
	printf("b - empurra inimigos \n");
	printf("v - mudar tipo de câmera \n");
	printf("botao mouse esquerdo - cria buraco \n");
	printf("botao mouse direito - destroi buracos \n");


}
///FUNÇÃO QUE LE OS BIMAPS DOS MAPAS E GERA UMA MATRIZ 20X20X4 CONTENDO OS VALORES RGBS DO BITMAP
void renderMaps(char mapName[], int matrixRGBA[20][20][4])
{
    printf ("\nLoading BMP models\n");
    // Load a texture object (256x256 true color)
    bits = LoadDIBitmap(mapName, &info);
    if (bits == (GLubyte *)0) {
		printf ("Error loading texture!\n\n");
		return;
	}
	rgba = (GLubyte *)malloc(info->bmiHeader.biWidth * info->bmiHeader.biHeight * 4);
    int space = 1;
    i = info->bmiHeader.biWidth * info->bmiHeader.biHeight-1;

    for( rgbaptr = rgba, ptr = bits;  i >= 0; i--, rgbaptr += 4, ptr += 3)
    {
            rgbaptr[0] = ptr[2];     // windows BMP = BGR
            rgbaptr[1] = ptr[1];
            rgbaptr[2] = ptr[0];
            rgbaptr[3] = 0;
    }
    i = info->bmiHeader.biWidth * info->bmiHeader.biHeight-1;
    int line = 19;
    int column = 0;

    for(rgbaptr = rgba; i >= 0; i--, rgbaptr += 4, space++, column++)
    {
        matrixRGBA[line][column][0] = rgbaptr[0];
        matrixRGBA[line][column][1] = rgbaptr[1];
        matrixRGBA[line][column][2] = rgbaptr[2];
        matrixRGBA[line][column][3] = rgbaptr[3];
        if (space%20 == 0)
        {
            line--;
            column = -1;
        }
    }
}
void initModel() {
	printf("Loading models.. \n");
	C3DObject_Load_New("Textures/Mario/mk_kart.obj",&modelOBJText);
	C3DObject_Load_New("Textures/Inimigos/Bowser/kk_kart.obj",&modelOBJInimigos);
	printf("Models ok. \n \n \n");
}
///INICIALIZAÇÃO DAS TEXTURAS DO JOGO
void initTexture(void)
{
    numTextures = 6;
    glGenTextures(numTextures, textures);
    loadTexture("res/grama1.bmp",0);
    loadTexture("res/terra.bmp",1);
    loadTexture("res/agua.bmp",2);
    loadTexture("res/pedra.bmp",3);
    loadTexture("res/grama_terra.bmp",4);
    loadTexture("res/buraco.bmp",5);

    for(int i=0;i<numTextures;i++)
    {
        printf("Textura %d\n", textures[i]);
        printf("Textures ok.\n\n", textures[i]);
    }
}
///INICIALIZAÇÃO DO SOM
void initSound() {

	printf("Initializing OpenAl \n");

	// Init openAL
	alutInit(0, NULL);

	alGetError(); // clear any error messages

    // Generate buffers, or else no sound will happen!
    alGenBuffers(NUM_BUFFERS, buffer);

    if(alGetError() != AL_NO_ERROR)
    {
        printf("- Error creating buffers !!\n");
        exit(1);
    }
    else
    {
        printf("init() - No errors yet.\n");
    }

	alutLoadWAVFile("res/Footsteps.wav",&format,&data,&size,&freq,false);
    alBufferData(buffer[0],format,data,size,freq);

	alGetError(); /* clear error */
    alGenSources(NUM_SOURCES, source);

    if(alGetError() != AL_NO_ERROR)
    {
        printf("- Error creating sources !!\n");
        exit(2);
    }
    else
    {
        printf("init - no errors after alGenSources\n");
    }

	listenerPos[0] = posX;
	listenerPos[1] = posY;
	listenerPos[2] = posZ;

	source0Pos[0] = posX;
	source0Pos[1] = posY;
	source0Pos[2] = posZ;

	alListenerfv(AL_POSITION,listenerPos);
    alListenerfv(AL_VELOCITY,listenerVel);
    alListenerfv(AL_ORIENTATION,listenerOri);

	alSourcef(source[0], AL_PITCH, 1.0f);
    alSourcef(source[0], AL_GAIN, 1.0f);
    alSourcefv(source[0], AL_POSITION, source0Pos);
    alSourcefv(source[0], AL_VELOCITY, source0Vel);
    alSourcei(source[0], AL_BUFFER,buffer[0]);
    alSourcei(source[0], AL_LOOPING, AL_TRUE);

	printf("Sound ok! \n\n");
}
//------------------------------------------------------------------
///FUNÇÃO QUE IRÁ DETERMINAR A DIREÇÃO EM QUE O INIMIGO IRA SE MOVIMENTAR
void definirDirecao()
{
    int direction;
    do
    {
        direction = 0;
        ///DETERMINAR A DIREÇÃO DE CADA INIMIGO, POIS ISSO IRÁ DEFINIR ONDE ELE IRÁ
        for(int i = 0; i<=3; i++)
        {
            direction = rand() % 4;
            switch(direction)
            {
                case 0: ///SUL Z+
                    posInimigos[i].direcaoX = 0;
                    posInimigos[i].direcaoZ = 2;
                break;
                case 1: ///DIREITA X+
                    posInimigos[i].direcaoX = 2;
                    posInimigos[i].direcaoZ = 0;
                break;
                case 2: ///NORTE Z-
                    posInimigos[i].direcaoX = 0;
                    posInimigos[i].direcaoZ = -2;
                break;
                case 3: ///ESQUERDA X-
                    posInimigos[i].direcaoX = -2;
                    posInimigos[i].direcaoZ = 0;
                break;
            }
        }
    }while(pathFinding());
}

void renderScene(bool minimapa) {
	glClearColor(backgrundColor[0],backgrundColor[1],backgrundColor[2],backgrundColor[3]);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	updateCam(minimapa);

	if(!inimigosAndando)
    {
        definirDirecao();
        auxGambi = 0;
        inimigosAndando = true;
    }
	else
    {
        movimentaInimigos();
        auxGambi++;
        if(auxGambi == 50)
        {
            inimigosAndando = false;
            auxGambi = 0;
        }
    }

    ///RENDERIZAÇÃO DOS MODELOS E DA LUZ PRINCIPAL
    glPushMatrix();
        randerBMPModels();
        glTranslatef(0.0,200.0,0.0);
        glLightfv(GL_LIGHT1, GL_POSITION, backgrundColor );
    glPopMatrix();
    glDisable(GL_TEXTURE_2D);

    // binds the bmp file already loaded to the OpenGL parameters
    glBindTexture(GL_TEXTURE_2D, textures[2]);
	renderFloor();

    ///PARTE DO CÓDIGO ONDE RENDERIZAMOS O PERSONAGEM JOGAVEL

        glPushMatrix();
            glScalef(0.2f,0.2f,0.2f);
            posXBoneco = posX*5;
            posZBoneco = posZ*5;
            posYBoneco = posY+2.48;

            glTranslatef(posXBoneco,posYBoneco,posZBoneco);

            mainChar.x = 20*((posXBoneco+19)/38);
            mainChar.z = 20*((posZBoneco+19)/38);

            defineDirecaoPersonagem();
            verificaEstadoJogo();
            verificaEstadoJogador();
        glPopMatrix();


}
///VERIFICAMOS QUAL É O ESTADO DO JOGADOR
void verificaEstadoJogador()
{
        if(empurrarMonstro)
        {
            terminouVerificacao = 0;
            empurrarDenovo = 0;
            empurrar();
        }
    if(!personagemMorto && !personagemMortoInimigo && contador != 0)
    {
        ///VERIFICAMOS SE O JOGADOR CRIAR UMA RACHADURA
        if(verificaBuraco == 1 && terminouVerificacao)
        {

            verificaBuraco = 0;
            terminouVerificacao = 0;
            criaRachadura();
        }
        ///FAZEMOS O FLOOD FILL
        if(testaFloodFill)
        {
            determinaSeed();
            //printf("x:%d z:%d", seedX, seedZ);
            floodFill(seedZ, seedX);
            ///SE HOUVER CICLO, DERRUBAMOS O CHAO
            if(determinaCiclo())
            {
                derrubaChao();
                limpaLixo();
            }
            ///REINICIALIZAMOS AS VARIAVEIS
            qntBlocosColoridos = 0;
            qntDeUns = 0;
            ciclo = false;
            testaFloodFill = false;
        }
        ///LIMPAMOS OS LIXOS DO MAPA
        ///ROTAMOS  O MODELO DO PERSONAGEM PRINCIPAL PARA UMA POSIÇÃO COERENTE COM A CAMERA
        glRotatef(rotCap,0,1,0);
        glmDraw(modelOBJText, GLM_SMOOTH | GLM_MATERIAL | GLM_TEXTURE);
        glPopMatrix();

        ///USADO PARA TRAVAR A CAMERA QUANDO MORREMOS
        posZAnterior = posZ;
        posXAnterior = posX;
        rotxAnterior = rotx;
        rotyAnterior = roty;
        rotCapAntigo = rotCap;
        placar();
    }
    else
    {///CASO O PERSONAGEM MORRA, ESSA FUNCAO FAZ A ANIMAÇÃO DA MORTE
        ///MOSTRA UMA MSG DE MORTE, E DPS DE 5 SEGUNDOS REINICIA CHAMANDO MAIN INIT;
        glPopMatrix();
        animacaoDeMorte();
    }
}
///FUNÇÃO QUE EMPURRA OS INIMIGOS
void empurrar()
{
    for(int i = 0 ; i <=3 ; i++)
    {
          if(((mainChar.x+mainChar.direcaoX == posInimigos[i].x)
          && (mainChar.z+mainChar.direcaoZ == posInimigos[i].y))

          || ((mainChar.x+mainChar.direcaoX+mainChar.direcaoZ/2 == posInimigos[i].x)
          && (mainChar.z+mainChar.direcaoZ+mainChar.direcaoX/2 == posInimigos[i].y))

          || ((mainChar.x+mainChar.direcaoX-mainChar.direcaoZ/2 == posInimigos[i].x)
          && (mainChar.z+mainChar.direcaoZ-mainChar.direcaoX/2 == posInimigos[i].y))

          || ((mainChar.x+mainChar.direcaoX/2 == posInimigos[i].x)
          && (mainChar.z+mainChar.direcaoZ/2 == posInimigos[i].y))

          || ((mainChar.x+mainChar.direcaoX/2+mainChar.direcaoZ/2 == posInimigos[i].x)
          && (mainChar.z+mainChar.direcaoZ/2+mainChar.direcaoX/2 == posInimigos[i].y))

          || ((mainChar.x+mainChar.direcaoX/2-mainChar.direcaoZ/2 == posInimigos[i].x)
          && (mainChar.z+mainChar.direcaoZ/2-mainChar.direcaoX/2 == posInimigos[i].y)))
          {
            if(((upperMap[posInimigos[i].y+mainChar.direcaoZ][posInimigos[i].x+mainChar.direcaoX][0] != 127
            && upperMap[posInimigos[i].y+mainChar.direcaoZ/2][posInimigos[i].x+mainChar.direcaoX/2][0] != 127)
               && (upperMap[posInimigos[i].y+mainChar.direcaoZ][posInimigos[i].x+mainChar.direcaoX][0] != 150
                   && upperMap[posInimigos[i].y+mainChar.direcaoZ/2][posInimigos[i].x+mainChar.direcaoX/2][0] != 150)
               && (upperMap[posInimigos[i].y+mainChar.direcaoZ][posInimigos[i].x+mainChar.direcaoX][0] != 90
                   && upperMap[posInimigos[i].y+mainChar.direcaoZ/2][posInimigos[i].x+mainChar.direcaoX/2][0] != 90))
               && posInimigos[i].y+mainChar.direcaoZ < 20 && posInimigos[i].y+mainChar.direcaoZ >=0
               && posInimigos[i].x+mainChar.direcaoX < 20 && posInimigos[i].x+mainChar.direcaoX >=0
               && (upperMap[posInimigos[i].y+mainChar.direcaoZ][posInimigos[i].x+mainChar.direcaoX][0] != 255))
            {
              upperMap[posInimigos[i].y+mainChar.direcaoZ][posInimigos[i].x+mainChar.direcaoX][0] = 255;
              upperMap[posInimigos[i].y][posInimigos[i].x][0] = 0;
            }
          }
    }
}
///CRIA UMA MENSAGEM NA TELA
void mensagem(char *p)
{
            glPushMatrix();
              glRotatef(rotCapAntigo,0,1,0);
                glTranslatef(+1.2,0.4,2);
                glScalef(0.006, 0.002, 0.002); // diminui o tamanho do fonte
                glLineWidth(5); // define a espessura da linha
                glRotatef(180,0,270,0);
                stroke_output(0, 0,0, p);
            glPopMatrix();
}
///VERIFICA OS PONTOS DO JOGADOR
void verificaPontos()
{
    pontuacao = 0;
    for(int i = 0; i<=3; i++)
    {
        if(posInimigos[i].x == -1 && posInimigos[i].y == -1)
        {
            pontuacao+=150;
        }
    }
}
///FUNÇÃO QUE IMPLEMENTA O DESAFIO EXTRA DE ELIMINAR RACHADURAS E BURACOS NO MEIO DA AGUA
void limpaLixo()
{
    for(int i = 0; i<20;i++)
    {
        for(int j = 0;j<20;j++)
        {///VERIFICA SE O LOWER MAP É AGUA, PEDRA, RACHADURA, BURACO OU INIMIGO
            if(lowerMap[i][j][3] == 0)
            {
                if(upperMap[i][j][0] == 90 || upperMap[i][j][0] == 150 || upperMap[i][j][0] == 127 || upperMap[i][j][0] == 255)
                {
                    if((lowerMap[i+1][j][1] == 0 && lowerMap[i-1][j][1] == 0 && lowerMap[i][j][3] == 0)
                       || (lowerMap[i][j+1][1] == 0 && lowerMap[i][j-1][1] == 0 && lowerMap[i][j][3] == 0))
                    {
                        lowerMap[i][j][0] = 0;
                        lowerMap[i][j][1] = 0;
                        lowerMap[i][j][2] = 0;
                        upperMap[i][j][0] = 0;
                        upperMap[i][j][1] = 0;
                        upperMap[i][j][2] = 0;
                    }
                }
            }
            lowerMap[i][j][3] = 0;
        }
    }
}
///FUNÇÃO QUE DERRUBA O CHÃO DO JOGO
void derrubaChao()
{
    PlaySound("res/blockDestruido.wav", NULL, SND_ASYNC|SND_FILENAME);
    if(qntBlocosColoridos>qntDeUns)
    {
        for(int i = 0; i<20;i++)
        {
            for(int j = 0;j<20;j++)
            {
                if(lowerMap[i][j][3] == 1)
                {
                    lowerMap[i][j][1] = 0;
                    upperMap[i][j][0] = 0;
                }
            }
        }
    }
    else
    {
        for(int i = 0; i<20;i++)
        {
            for(int j = 0;j<20;j++)
            {
                if(lowerMap[i][j][3] == 255)
                {
                    lowerMap[i][j][1] = 0;
                    upperMap[i][j][0] = 0;
                }
            }
        }
    }
}
///ALGORITMO QUE DETERMINA SE EXISTE ALGUMA REGIÃO QUE DEVE SER DEMOLIDA
bool determinaCiclo()
{
    ciclo = false;
    for(int i = 0; i<20;i++)
    {
        for(int j = 0;j<20;j++)
        {
            if(lowerMap[i][j][3] == 1)
            {
                qntDeUns++;
                ciclo = true;
            }

        }
    }
    return ciclo;
}
///ALGORITMO DE FLOOD FILL
bool floodFill(int seedz, int seedx)
{
    if(upperMap[seedz][seedx][0] == 150 && lowerMap[seedz][seedx][1] == 255 ///É BURACO
       || upperMap[seedz][seedx][0] == 90 && lowerMap[seedz][seedx][1] == 255///É RACHADURA
       || lowerMap[seedz][seedx][1] == 0///SE ELE FOR AGUA
       || lowerMap[seedz][seedx][3] == 255)///JA FOI PINTADO
    {
        return false;
    }
    if((upperMap[seedz][seedx][0] == 0 || upperMap[seedz][seedx][0] == 255) && lowerMap[seedz][seedx][1] == 255)
    {
        lowerMap[seedz][seedx][3] = 255 ;
        qntBlocosColoridos++;
        floodFill(seedz-1,seedx);
        floodFill(seedz+1,seedx);
        floodFill(seedz,seedx+1);
        floodFill(seedz,seedx-1);

        return false;
    }
}
///FUNÇÃO QUE DETERMINA A SEED DO ALGORITMO DE FLOOD FILL
void determinaSeed()
{
    if(upperMap[mainChar.z+1][mainChar.x][0] == 0 && lowerMap[mainChar.z+1][mainChar.x][1] == 255)
    {
        seedX = mainChar.x;
        seedZ = mainChar.z+1;
    }
    else if(upperMap[mainChar.z-1][mainChar.x][0] == 0 && lowerMap[mainChar.z-1][mainChar.x][1] == 255)
    {
        seedX = mainChar.x;
        seedZ = mainChar.z-1;
    }
    else if(upperMap[mainChar.z][mainChar.x+1][0] == 0 && lowerMap[mainChar.z][mainChar.x+1][1] == 255)
    {
        seedX = mainChar.x+1;
        seedZ = mainChar.z;
    }
    else if(upperMap[mainChar.z][mainChar.x-1][0] == 0 && lowerMap[mainChar.z][mainChar.x-1][1] == 255)
    {
        seedX = mainChar.x-1;
        seedZ = mainChar.z;
    }
    else if(upperMap[mainChar.z+1][mainChar.x+1][0] == 0 && lowerMap[mainChar.z+1][mainChar.x+1][1] == 255)
    {
        seedX = mainChar.x+1;
        seedZ = mainChar.z+1;
    }
    else if(upperMap[mainChar.z-1][mainChar.x-1][0] == 0 && lowerMap[mainChar.z-1][mainChar.x-1][1] == 255)
    {
        seedX = mainChar.x-1;
        seedZ = mainChar.z-1;
    }
    else if(upperMap[mainChar.z-1][mainChar.x+1][0] == 0 && lowerMap[mainChar.z-1][mainChar.x+1][1] == 255)
    {
        seedX = mainChar.x+1;
        seedZ = mainChar.z-1;
    }
    else if(upperMap[mainChar.z+1][mainChar.x-1][0] == 0 && lowerMap[mainChar.z+1][mainChar.x-1][1] == 255)
    {
        seedX = mainChar.x-1;
        seedZ = mainChar.z+1;
    }
}
///VERIFICA E CRIA AS RACHADURAS NO CHÃO
void criaRachadura()
{
    naoAcabou = true;
    andarZ = 0;
    andarX = 0;
    ///É BURACO?
    printf("gridX: %d, gridZ: %d \n", mainChar.direcaoX, mainChar.direcaoZ);
    if(upperMap[mainChar.z][mainChar.x][0] == 150)
    {
        ///ENTAO VAMO PROCURAR SE A RACHADURA PODE SER CONSTRUIDA
        while(naoAcabou)
        {
            andarZ += mainChar.direcaoZ/2;
            andarX += mainChar.direcaoX/2;
            ///VERIFICA SE ACHAMOS ALGUMA RACHADURA, BURACO OU FINAL DO MAPA
            if(upperMap[mainChar.z+andarZ][mainChar.x+andarX][0] == 150
               || upperMap[mainChar.z+andarZ][mainChar.x+andarX][0] == 90
               || lowerMap[mainChar.z+andarZ][mainChar.x+andarX][1] == 0)
            {
                andarZ = 0;
                andarX = 0;
                ///SE ACHARMOS, VAMOS CRIAR RACHADURA ATÉ ESSE PONTO
                while(naoAcabou)
                {
                    printf("gridX: %d, gridZ: %d \n", mainChar.x+andarX, mainChar.z+andarZ);
                    andarZ += mainChar.direcaoZ/2;
                    andarX += mainChar.direcaoX/2;
                    ///POSIÇÃO VAZIA VIRA RACHADURA
                    if(upperMap[mainChar.z+andarZ][mainChar.x+andarX][0] != 150
                       && upperMap[mainChar.z+andarZ][mainChar.x+andarX][0] != 90
                       && lowerMap[mainChar.z+andarZ][mainChar.x+andarX][1] != 0)
                    {
                        upperMap[mainChar.z+andarZ][mainChar.x+andarX][0] = 90;
                        upperMap[mainChar.z+andarZ][mainChar.x+andarX][1] = 50;
                        upperMap[mainChar.z+andarZ][mainChar.x+andarX][2] = 50;
                        lowerMap[mainChar.z+andarZ][mainChar.x+andarX][3] = 0;
                    }
                    else
                    {///RESETAMOS AS VARIAVEIS DE VERIFICAÇÃO
                        testaFloodFill = true;
                        naoAcabou = false;
                    }
                }
                naoAcabou = false;
            }///ACHOU PEDRA, ENTAO FAZ NADA
            if(upperMap[mainChar.z+andarZ][mainChar.x+andarX][0] == 127 || upperMap[mainChar.z+andarZ][mainChar.x+andarX][0] == 255)
            {
                naoAcabou = false;
            }
        }
    }
}
///FUNÇÃO QUE MAPEIA OS MODELOS DA MATRIZ RGB DE BITMAP PARA O UNIVERSO DO JOGO
void randerBMPModels()
{
    for(int auxZerar = 0; auxZerar <=3; auxZerar++)
    {
        posInimigos[auxZerar].x = -1;
        posInimigos[auxZerar].y = -1;
    }
    int pontoAux = 0;
    zAux = -20;
    for(int i2=0;i2<20;i2++)
    {
        xAux = -20;
        for(int j=0;j<20;j++)
        {///VERMELHO = INIMIGOS
            if(((upperMap[i2][j][0] == 255) && (upperMap[i2][j][1] == 0) && (upperMap[i2][j][2] == 0))
               && (lowerMap[i2][j][1] == 255) && (lowerMap[i2][j][2] == 0))
            {
                if(pontoAux <= 3)
                {
                    posInimigos[pontoAux].x = j;
                    posInimigos[pontoAux].y = i2;
                    glPushMatrix();
                        glScalef(0.2,0.2,0.2);
                        glTranslatef(xAux+1+posInimigos[pontoAux].offsetX,altura+3.0,zAux+1+posInimigos[pontoAux].offsetZ); //CULPA DAS COORDENADAS DO OBJ
                        rotaInimigos(pontoAux);
                        glmDraw(modelOBJInimigos, GLM_SMOOTH | GLM_MATERIAL | GLM_TEXTURE);
                    glPopMatrix();
                    glDisable(GL_TEXTURE_2D);
                    pontoAux++;
                }
                else
                {
                    upperMap[i2][j][0] = 0;
                }
            }///MARROM = RACHADURA
            if (((upperMap[i2][j][0] == 90) && (upperMap[i2][j][1] == 50) && (upperMap[i2][j][2] == 50))
                && ((lowerMap[i2][j][1] == 255) && (lowerMap[i2][j][2] == 0)))
            {   ///GAMBIARRA PARA FAZER A DESTRUIÇÃO DO CHÃO
                lowerMap[i2][j][3] == 0;
                glPushMatrix();
                    glScalef(0.2,0.2,0.2);
                    glTranslatef(xAux,altura,zAux);
                    renderCubes(4,4);
                glPopMatrix();
            }
            else
            {///ESSE MARROM CLARO = BURACO
                if (((upperMap[i2][j][0] == 150) && (upperMap[i2][j][1] == 120) && (upperMap[i2][j][2] == 80))
                    && (lowerMap[i2][j][1] == 255) && (lowerMap[i2][j][2] == 0))
                {   ///GAMBIARRA PARA FAZER A DESTRUIÇÃO DO CHÃO
                    lowerMap[i2][j][3] == 0;
                    glPushMatrix();
                        glScalef(0.2,0.2,0.2);
                        glTranslatef(xAux,altura,zAux);
                        renderCubes(5,4);
                    glPopMatrix();
                }
                else
                {///GREEN = GRAMA
                    if((lowerMap[i2][j][1] == 255) && (lowerMap[i2][j][2] == 0))
                    {   ///GAMBIARRA PARA FAZER A DESTRUIÇÃO DO CHÃO
                        lowerMap[i2][j][3] = 1;
                        glPushMatrix();
                            glScalef(0.2,0.2,0.2);
                            glTranslatef(xAux,altura,zAux);
                            renderCubes(0,1);
                        glPopMatrix();
                    }
                    ///CINZA = PEDRA
                    if(((upperMap[i2][j][0] == 127) && (upperMap[i2][j][1] == 127) && (upperMap[i2][j][2] == 127))
                       && (lowerMap[i2][j][1] == 255) && (lowerMap[i2][j][2] == 0))
                    {   ///GAMBIARRA PARA FAZER A DESTRUIÇÃO DO CHÃO
                        lowerMap[i2][j][3] = 0;
                        glPushMatrix();
                            glScalef(0.2,0.1,0.2);
                            glTranslatef(xAux,altura+4,zAux);
                            renderCubes(3,3);
                        glPopMatrix();
                        glDisable(GL_TEXTURE_2D);
                    }
                }
            }
            xAux+=2;
        }
        zAux+=2;
    }
}
//-----------------------------------------------------------------------------------
///UPDATE STATES DO JOGO
void updateState() {
    ///SUAVIZAÇÃO DE CAMERA PARA ESQUERDA
    if(rotateA == true)
        {///SUAVIZAÇÃO DIFERENCIADA DO OBJ DO MARIO
            if(auxCap != 90*qntRotate)
            {
                rotCap += 30;
                auxCap += 30;
            }
            ///SUAVIZAÇÃO DA CAMERA EM SI
            if(auxA != -90*qntRotate)
            {
                roty -= 10;
                auxA -= 10;
            }
            else
            {
                rotateA = false;
                qntRotate = 0;
            }
        }
    ///SUAVIZAÇÃO DE CAMERA PARA A DIREITA
    if(rotateD == true)
        {///SUAVIZAÇÃO DIFERENCIADA DO OBJ DO MARIO
            if(auxCap != -90*qntRotate)
            {
                rotCap -= 30;
                auxCap -= 30;
            }
            ///SUAVIZAÇÃO DA CAMERA EM SI
            if(auxA != +90*qntRotate)
            {
                roty += 10;
                auxA += 10;
            }
            else
            {
                rotateD = false;
                qntRotate = 0;
            }
        }
	if (upPressed || downPressed) {
		if (running) {
			speedX = 0.045 * sin(roty*PI/180) * 2;
			speedZ = -0.045 * cos(roty*PI/180) * 2;
		} else {
			speedX = 0.06 * sin(roty*PI/180);
			speedZ = -0.06 * cos(roty*PI/180);
		}
		// efeito de "sobe e desce" ao andar
		headPosAux += 8.5f;
		if (headPosAux > 180.0f) {
			headPosAux = 0.0f;
		}
        if (upPressed) {
            posX += speedX;
            posZ += speedZ;
            verificaColisao(posX, posZ, -speedX, -speedZ);
        } else {
            posX -= speedX;
            posZ -= speedZ;
            verificaColisao(posX, posZ, speedX, speedZ);
        }
	} else {
		// parou de andar, para com o efeito de "sobe e desce"
		headPosAux = fmod(headPosAux, 90) - 1 * headPosAux / 90;
		headPosAux -= 4.0f;
		if (headPosAux < 0.0f) {
			headPosAux = 0.0f;
		}
	}

	posY += speedY;
}
///ALGORITMO NAIVE DE COLISÃO DO PERSONAGEM PRINCIPAL COM OS OBJETOS DO JOGO
void verificaColisao(float posx, float posz, float speedXis, float speedZe)
{
    XBoneco = posx*5;
    ZBoneco = posz*5;
    float faixaDeColisao = 1.55; //PEDRAS
    float faixaDeColisaoAgua = 1.0;

    ///FOR QUE IRA VERIFICAR TODOS OS PONTOS DO GRID PARA COLISÃO
    zAux = -19;
    for(int i2=0;i2<20;i2++)
    {
        xAux = -19;
        for(int j=0;j<20;j++)
        {///VERIFICA COLISAO COM AS PEDRAS E O CHÃO CASO ELE TENHA CAÍDO DO MAPA
            if (((upperMap[i2][j][0] == 127) && (upperMap[i2][j][1] == 127) && (upperMap[i2][j][2] == 127))
                && ((lowerMap[i2][j][0] == 0) && (lowerMap[i2][j][1] == 255) && (lowerMap[i2][j][2] == 0))
                || ((lowerMap[i2][j][0] == 0) && (lowerMap[i2][j][1] == 255) && (lowerMap[i2][j][2] == 0) && posYBoneco < 2.88))
            {///ELE CRIA UMA BOX ENVOLTA DA PEDRA E CALCULA SE A POS DO PERSONAGEM ESTÁ DENTRO DESSA BOX DE VALORES [-1,1]
                if((XBoneco >= xAux-faixaDeColisao) && (XBoneco <= xAux+faixaDeColisao)
                   && (ZBoneco >= zAux-faixaDeColisao) && (ZBoneco <= zAux+faixaDeColisao))
                {
                    j = 20;
                    i2 = 20;
                    posX = posX + speedXis;
                    posZ = posZ + speedZe;
                }
            }///VERIFICA COLISÃO COM A AGUA
            if ((lowerMap[i2][j][0] == 0) && (lowerMap[i2][j][1] == 0) && (lowerMap[i2][j][2] == 0))
            {///ELE CRIA UMA BOX ENVOLTA DA AGUA E INIMIGOS PARA CALCULAR SE A POS DO PERSONAGEM ESTÁ DENTRO DESSA BOX DE VALORES [-1,1]
                if((XBoneco >= xAux-faixaDeColisaoAgua) && (XBoneco <= xAux+faixaDeColisaoAgua)
                   && (ZBoneco >= zAux-faixaDeColisaoAgua) && (ZBoneco <= zAux+faixaDeColisaoAgua))
                {
                    j = 20;
                    i2 = 20;
                    personagemMorto = true;
                }
            }
            if((upperMap[i2][j][0] == 255) && (upperMap[i2][j][1] == 0) && (upperMap[i2][j][2] == 0))
            {///ELE CRIA UMA BOX ENVOLTA DA AGUA E INIMIGOS PARA CALCULAR SE A POS DO PERSONAGEM ESTÁ DENTRO DESSA BOX DE VALORES [-1,1]
                if((XBoneco >= xAux-faixaDeColisaoAgua) && (XBoneco <= xAux+faixaDeColisaoAgua)
                   && (ZBoneco >= zAux-faixaDeColisaoAgua) && (ZBoneco <= zAux+faixaDeColisaoAgua))
                {
                    j = 20;
                    i2 = 20;
                    personagemMortoInimigo = true;
                }
            }
            if(XBoneco <= -19.2 || XBoneco >=19.2 || ZBoneco <= -19.2 || ZBoneco >= 19.2)
            {
                j = 20;
                i2 = 20;
                posX = posX + speedXis;
                posZ = posZ + speedZe;
            }
            xAux+=2;
        }
        zAux+=2;
    }

}
///MAIN RENDER E DAS VIEWPORTS DO MINIMAPA E MAPA PRINCIPAL
void mainRender() {
    updateState();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  // limpar o depth buffer
    ///VIEWPORT PRINCIPAL
    glViewport(0, 0, windowWidth, windowHeight);
        renderScene(TRUE);
    glClear(GL_DEPTH_BUFFER_BIT);
    ///VIEWPORT QUE CRIA O MINIMAPA QND FOR VISAO FPS OU 3RD PERSON
    if(visao != 2)
    {
        glViewport(3*windowWidth/4, 3*windowHeight/4, windowWidth/4, windowHeight/4);//
        renderScene(FALSE);
    }

    glFlush();
    glutPostRedisplay();
	Sleep(30);
	glutSwapBuffers();
}
///FUNÇÃO QUE FAZ AS ANIMAÇÕES E COMANDOS DE VITÓRIA
void vitoria()
{
    if(restart == 0)
    {
        PlaySound("res/win.wav", NULL, SND_SYNC|SND_FILENAME);
    }
    if(restart == 0){

        mainInit();
    }
}
///HANDLER DE KEY PRESSED
void onKeyDown(unsigned char key, int x, int y) {
	//printf("%d \n", key);
	switch (key) {
        case 32: //space
            if(rotateA != true && rotateD != true)
                PlaySound("res/britadeira.wav", NULL, SND_ASYNC|SND_FILENAME);
                verificaBuraco = 1;
            break;
		case 119: //w
			if (!upPressed) {
				alSourcePlay(source[0]);
			}
			upPressed = true;
			break;
		case 115: //s
			downPressed = true;
			break;
		case 97: //a
		    if(rotateD == false)
            {
                rotateA = true;
                if(qntRotate == 0)
                {
                    auxCap = 0;
                    auxA = 0;
                }
                qntRotate +=1;
            }
			break;
		case 100: //d
		    if(rotateA == false)
            {
                rotateD = true;
                if(qntRotate == 0)
                {
                    auxA = 0;
                    auxCap = 0;
                }
                qntRotate +=1;
            }
			break;
        case 98: //b
            PlaySound("res/empurra.wav", NULL, SND_ASYNC|SND_FILENAME);
            empurrarMonstroTimer = true;
            empurrarMonstro = true;
            break;
		case 114: //r
            running = true;
			break;

        case 118: //v
            ///VARIAVEL AUX QUE DEFINE QUAL SERÁ O TIPO DE CAMERA USADA
            if(visao == 2)
            {
                visao = 0;
            }
            else
            {
                visao++;
            }
            break;
		default:
			break;
	}

	//glutPostRedisplay();
}
///HANDLER DE KEY RELEASE
void onKeyUp(unsigned char key, int x, int y) {
	switch (key) {
		case 32: //space
			verificaBuraco = 0;
			terminouVerificacao = true;
			break;
		case 119: //w
			if (upPressed) {
				alSourceStop(source[0]);
			}
			upPressed = false;
			break;
		case 115: //s
			downPressed = false;
			break;
        case 98: //b
            terminouVerificacao = true;
            empurrarMonstro = false;
            break;
		case 114: //r
			running = false;
			break;
		case 27:
			exit(0);
			break;
		default:
			break;
	}

	//glutPostRedisplay();
}
///INT MAIN
int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(windowWidth,windowHeight);
	glutInitWindowPosition(windowXPos,windowYPos);

	/**
	Store main window id so that glui can send it redisplay events
	*/
	mainWindowId = glutCreateWindow("FPS");

    glutDisplayFunc(mainRender);

	glutReshapeFunc(onWindowReshape);

	/**
	Register mouse events handlers
	*/
    glutMouseFunc(onMouseButton);

	/**
	Register keyboard events handlers
	*/
	glutKeyboardFunc(onKeyDown);
	glutKeyboardUpFunc(onKeyUp);

    glutTimerFunc(33, Timer, 1);

    mainInit();

	glutMainLoop();

    return 0;
}
